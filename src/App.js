import React, { Component, Fragment } from 'react';
import './App.css';
import Header from './Header'
import * as actions from './redux/actions/action1.js';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      check1 : false,
      check2 : false,
      check3 : false,
      check4 : false,
      check7 : "",
      ad: "Example",
      degistir : "İsim",
      soyad: 1,
      hakkinda: [
                {
                  Adi: "Mert",
                },
                {
                  Soyadi: "Yakdemir"
                }
              ],
        list: [ {
         aciklama1 : "List1",
         aciklama2 : "List2"
        }
        ],
        keys : {
          aciklama3 : "keys1",
          aciklama4 : "keys2",
        }
       }
     }

    onclick=()=>
    {
      actions.setR1Name("Mert");
    }
    onclick2=()=>
    {
      actions.setR1Value("1");
    }
    degis=(e)=>
    {
      this.setState({ degistir : "Mert" })
    }
    oncheck=(e)=>
    {
      this.setState({ check1 : !this.state.check1})
    }
    oncheck2=(e)=>
    {
      this.setState({ check2 : !this.state.check2})
    }
    oncheck3=(e)=>
    {
      this.setState({ check3 : !this.state.check3})
    }
    oncheck4=(e)=>
    {
      this.setState({ check4 : !this.state.check4})
    }
    oncheck7=(e)=>
    {
      this.setState({ check7 : e.target.value })
    }

    render() {
    var check5 = "" , check6 = 0
    if( this.state.check1 === true) {
      check6 = this.state.check7 * 7.7 / 100;
      check5 = 4.85 * check6;
    }
    const { ad } = this.state;
    const list = this.state.list;
    const aciklamaList = list.map((p,i)=>
    <div key={i}>
     <p>{p.aciklama1}</p>
     <p>{p.aciklama2}</p>
     </div>
    )
    const { aciklama3,aciklama4} = this.state.keys;
    const hakkinda = this.state.hakkinda;
    let names = [];
    hakkinda.map((e,i) => names.push(Object.keys(hakkinda[i])));
    return (
      <div className="App">
      {hakkinda.map((e,i)=>
      <Fragment key={i}>
      { names[i][0] } {' '}
      { e[names[i][0]] } {' '}
      </Fragment>
      )}
      <p>
      <button onClick={this.onclick}>Update</button>
      <br />
      <button onClick={this.onclick2}>Update2</button>

      </p>
          <Header/>
          {ad}
          {aciklamaList}
          <p>{aciklama3}</p>
          <p>{aciklama4}</p>
          <button onClick={this.degis}>Tıkla</button>
          {this.state.degistir}
          <br />
          <input type="checkbox" checked={this.state.check1} onChange={this.oncheck}/> <br />
          <input type="checkbox" checked={this.state.check2} onChange={this.oncheck2}/> <br />
          <input type="checkbox" checked={this.state.check3} onChange={this.oncheck3}/> <br />
          <input type="checkbox" checked={this.state.check4} onChange={this.oncheck4}/> <br />
          <input type="text" onChange={this.oncheck7} value={this.state.check7} /> <br />
          <p> Sonuç  {check5}  </p>
      </div>
    );
  }
}

export default App;
