import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import reducersCombined from './redux/reducers';
import {createStore} from 'redux';

export const store = createStore(reducersCombined);

ReactDOM.render(<App />, document.getElementById('root'));
registerServiceWorker();
