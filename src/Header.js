import React, { Component } from 'react';
import {store} from './index.js';

export class Header extends Component {
  constructor(props){
    super(props);
    this.connect(['reducer1','reducer2']);
  }
  connect(reducers =[] )
  {
    this.storeSubscription = store.subscribe( this.storeListener.bind(this,reducers, true));
    this.storeListener(reducers,false);
  }
  storeListener(reducers =[], isMounted)
  {
    var state = store.getState();
    var stateObjects = {};
    for(var i=0; i<reducers.length;i++)
    {
      stateObjects = Object.assign( {}, stateObjects, { ...state[reducers[i]] } );
    }
    if(isMounted) {
      this.setState(stateObjects);
    }
    else {
      this.state = stateObjects;
    }
  }

  componentWillUnmount() {
    if(this.storeSubscription){
      this.storeSubscription();
    }
  }

   render() {
    return (
      <div>
        <h1>
       {this.state.R1Name} {''} {this.state.R2Name}
       </h1>
       <h3>
         {this.state.R1Value}
      </h3>
      </div>
    );
  }
}

export default Header;
