import {store} from '../../index.js';

export function setR1Name(R1Name) {
    store.dispatch({
        type: 'SET_R1Name',
        payload: {
            R1Name
        }
    });
}

export function setR1Value(R1Value) {
    store.dispatch({
        type: 'SET_R1Value',
        payload: {
            R1Value
        }
    });
}

// fonksiyon ekleyebiliriz