var initialState = {
    R1Name : 'İsminiz',
    R1Value : 0
}

export default function reducer1(state = initialState, action)
{
switch(action.type) {
    case 'SET_R1Name':
    return Object.assign({} ,state, { R1Name: action.payload.R1Name });
    case 'SET_R1Value':
    return Object.assign({} ,state, { R1Value: action.payload.R1Value });
    default:
    return state;
  }
}